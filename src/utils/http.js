import Taro from '@tarojs/taro';
import url from '../constants/api';
export const getRequest =  (url, data) => {
    return Taro.request({
        url,
        data,
        method: 'GET',
        success: function (res) {
            console.log('res', res)
        }
    })
}
export const postRequest = (url, data) => {
    Taro.request({
        url, 
        data,
        method: 'POST'
    })
}

export const getTopics = async () => {
    console.log(url.getTopics)
    let result = await getRequest(url.getTopics, {
        page: 1,
        tab: 'share',
        limit: 10
    }).catch(err => {
        throw new Error('出错了')
    });
    return result;
}