const baseURL = 'https://cnodejs.org/api/v1';
const obj = {
    getTopics: baseURL + '/topics', // 获取话题列表
    getTopicInfo: baseURL + '/topic', // 获取话题详情
    createTopics: baseURL + '/topics', // 新建话题
    getUserInfo: baseURL +'/user', // 获取用户信息
    checkUserToken: baseURL + '/accesstoken', // 严重用户token
    replyTopic: baseURL + '/topic', // 回复话题消息
    upReply: baseURL + '/reply'
}
export default obj;