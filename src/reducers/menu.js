const MENU_DATA = {
    menuData: [{
        key: 'all',
        value: '全部',
    },{
        key: 'share',
        value: '分享',
    },{
        key: 'good',
        value: '精华',
    },{
        key: 'ask',
        value: '问答',
    },{
        key: 'job',
        value: '招聘',
    },{
        key: 'dev',
        value: '客户端测试'
    }],
    showMenu: false,
    currentCata: {name: '首页', key: 'all'}
}
export default function menu (state = MENU_DATA, action) {
    switch (action.type) {
        case 'showDrawer': 
            return {
                ...state,
                showMenu: true
            }
        case 'hiddenDrawer':
            return {
                ...state,
                showMenu: false
            }
        case 'changeTitle':
            return {
                ...state,
                currentCata: {name: action.currentCata.value, key: action.currentCata.key}
            }
        default: 
            return {
                ...state
            }
    }
}