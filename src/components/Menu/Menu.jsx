import {View,  Image, Text} from '@tarojs/components';
import './index.less'
import {openDrawer, closeDrawer, changeCata} from '../../actions/menu'
import { AtDrawer } from 'taro-ui'
import { connect } from 'react-redux';
import { Component } from 'react'
const leftIcon = require("../../assets/images/cata.png")
const rightIcon = require("../../assets/images/login.png")

@connect(function(store){return {...store.menu}},function(dispatch) {
    return {
        openDrawer() {
        dispatch(openDrawer())
    },
    closeDrawer() {
        dispatch(closeDrawer())
    },
    changeCata(cata) {
        dispatch(changeCata(cata))
    }
}
})
export default class Menu extends Component {
    handleClick = () => {
        // const {  menuData } = this.props;
            this.props.openDrawer()
    }
    getItems = () => {
        const {  menuData } = this.props;
        return menuData.map(item => item.value)
    }
    itemClick = (index) => {
        const {  menuData } = this.props;
        this.props.changeCata(menuData[index])
    }
    render() {
        const { showMenu, currentCata } = this.props;
        console.log('showMenu', showMenu)
        return  (
            <View className="top">
                <AtDrawer onClose={this.props.closeDrawer} style={{position: 'absolute'}} show={showMenu}  items={this.getItems()} onItemClick={this.itemClick}/>
                <Image className="left"  onClick={this.handleClick} style={{width: '28PX', height: '28PX'}} src={leftIcon}/>
        <Text className="text">{currentCata.name}</Text>
                <Image  className="right" style={{width: '28PX', height: '28PX'}} src={rightIcon}/>
            </View>
        )
    }
}