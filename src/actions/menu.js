
  export const openDrawer = () => {
    return dispatch => {
      dispatch({type: 'showDrawer'})
    }
  }
  export const closeDrawer = () => {
    return dispatch => {
      dispatch({type: 'hiddenDrawer'})
    }
  }
  export const changeCata = (cata) => {
    return dispatch => {
      dispatch({type: 'changeTitle', currentCata: cata})
    }
  }
